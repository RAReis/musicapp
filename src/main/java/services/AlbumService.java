package services;

import models.Album;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class AlbumService {

    static List<Album> albumList = new LinkedList<>();

    public static void addAlbum(Album album){
        albumList.add(album);
    }

    public static void addAlbumList(Collection<Album> albumCollection){
        for (Album a : albumCollection) {
            addAlbum(a);
        }
    }

    public static List<Album> getAlbumList() {
        return albumList;
    }

    public static void printAlbums(){
        for (Album a : albumList) {
            System.out.printf("Album -> ID: %d  NAME: \" %s \"\n", a.getId(), a.getName());
        }
    }
}
