package services;

import models.Track;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TrackService {

    static List<Track> trackList = new LinkedList<>();

    public static void addTrack(Track track){
        trackList.add(track);
    }

    public static void addTrackList(Collection<Track> trackCollection){
        for (Track t : trackCollection) {
          addTrack(t);
        }
    }

    public static List<Track> getTrackList() {
        return trackList;
    }

    public static void printTracks(){
        for (Track t : trackList) {
            System.out.printf("Track -> ID: %d  NAME: \" %s \"\n", t.getId(), t.getName());
        }
    }

}
