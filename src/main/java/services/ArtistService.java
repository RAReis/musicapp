package services;

import models.Artist;

import java.util.*;

public class ArtistService {

    static List<Artist> artistList = new LinkedList<>();

    public static void addArtist(Artist artist){
        artistList.add(artist);
    }

    public static void addArtistList(Collection<Artist> artistCollection){
        for (Artist a : artistCollection) {
            addArtist(a);
        }
    }

    public static List<Artist> getArtistList() {
        return artistList;
    }

    public static void printArtists(){
        for (Artist a : artistList) {
            System.out.printf("Artist -> ID: %d  NAME: \" %s \"\n", a.getId(), a.getName());
        }
    }

    public static Artist getArtistById(int artID){
        for (Artist a : artistList) {
            if(a.getId() == artID){
                return a;
            }
        }

        return null;
    }
}
