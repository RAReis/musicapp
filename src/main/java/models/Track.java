package models;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Track {
    static int idGenerator = 1;
    private int trackId;
    private String name;
    private int duration; // in seconds
    private String genre;
    private List<Album> albumList;
    private List<Artist> artistList;

    public Track(String name, int duration, String genre, Collection<Artist> artists) {
        trackId = idGenerator++;
        this.name = name;
        this.duration = duration;
        this.genre = genre;
        albumList = new LinkedList<Album>();
        artistList = new LinkedList<Artist>();
        artistList.addAll(artists);
    }

    public String getName() {
        return name;
    }

    public double getDuration() {
        return duration;
    }

    public String getGenre() {
        return genre;
    }

    public List<Album> getAlbumList() {
        return albumList;
    }

    public List<Artist> getArtistList() {
        return artistList;
    }

    public int getId() {
        return trackId;
    }

    public void printArtistList(){
            for (Artist a : artistList) {
                System.out.printf("\n [Artist name: %s]",a.getName());
            }
    }

    public void printAlbumList(){
        for (Album a : albumList) {
            System.out.printf("\n [Album name: %s]",a.getName());
        }
    }

    public void addToAlbumList(Album album){
        albumList.add(album);
    }
}
