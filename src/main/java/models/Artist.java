package models;

import java.util.LinkedList;
import java.util.List;

public class Artist {
    private static int idGenerator = 1;
    private int artistId;
    private String name;
    private List<Track> trackList;

    public Artist(String name) {
        artistId = idGenerator++;
        this.name = name;
        this.trackList = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return artistId;
    }

    public void addTrackToArtist(Track track) {
        this.trackList.add(track);
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void printTrackList() {
        for (Track track : trackList) {
            System.out.printf("\n\t\t [ Track name: %s ]\n", track.getName());
        }
    }
}
