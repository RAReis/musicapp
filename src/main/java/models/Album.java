package models;

import java.util.LinkedList;
import java.util.List;

public class Album {
    private static int idGenerator = 1;
    private int albumId;
    private int year;
    private String name;
    private List<Track> trackList;
    private List<String> genreList;

    public Album(int year, String name) {
        albumId = idGenerator++;

        this.year = year;
        this.name = name;
        this.trackList = new LinkedList<>();
        this.genreList = new LinkedList<>();

    }

    public int getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return albumId;
    }

    public void addTrackToAlbum(Track track){
        this.trackList.add(track);
        this.genreList.add(track.getGenre());
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public List<String> getGenreList() {
        return genreList;
    }

    public void printTrackList(){
        for (Track track: trackList) {
            System.out.printf("\n\t\t\t [ Track name: %s ]",track.getName());
        }
    }

    public void printGenres(){
        for (String genre : genreList) {
            System.out.printf("\nGenre: %s", genre);
        }
    }
}
