package menus.search;

import menus.BaseMenu;
import services.ArtistService;
import menus.UserInput;
import models.Artist;

import java.util.List;

public class EntitySearchArtist extends BaseMenu {
    private UserInput userInput = new UserInput();


    public void showAndChooseArtist() {
        clearScreen();
        printHeader();
        List<Artist> artists = ArtistService.getArtistList();
        int artistCount = 0;

        if (artists.isEmpty()) {
            System.out.println("\nNo Artists found");
            userInput.goBack();
            return;
        }

        ArtistService.printArtists();

        System.out.print("\nChoose using Name: ");
        String name = userInput.getName();

        for (Artist artist : artists) {
            if (artist.getName().toLowerCase().contains(name.toLowerCase())) {
                System.out.printf("\n[Artist ID: %d]\n [Artist Name: %s]", artist.getId(), artist.getName());
                artist.printTrackList();
                artistCount++;
            }
        }

        if (artistCount == 0) {
            System.out.println("\nNo artists found with that name");
        }

        userInput.goBack();
    }

    public void printHeader(){
        System.out.println("\t>---------------<");
        System.out.println("\t>  Artist List  <");
        System.out.println("\t>---------------<");
    }

}
