package menus.search;

import menus.BaseMenu;
import menus.UserInput;
import models.Album;
import models.Track;
import services.TrackService;

import java.util.List;

public class EntitySearchTrack extends BaseMenu {
    private UserInput userInput = new UserInput();

    public void showAndChooseTrack() {
        clearScreen();
        printHeader();
        List<Track> tracks = TrackService.getTrackList();
        int trackCount = 0;

        if (tracks.isEmpty()) {
            System.out.println("\nNo Tracks found");
            userInput.goBack();
            return;
        }

        TrackService.printTracks();

        System.out.print("\nChoose using Name: ");
        String name = userInput.getName();

        for (Track track : tracks) {
            if (track.getName().toLowerCase().contains(name.toLowerCase())) {
                System.out.printf("\n[ Track ID: %d ] \nTrack NAME: %s", track.getId(), track.getName());
                List<Album> albums = track.getAlbumList();
                System.out.println("\n Albums :");
                for (Album a : albums) {
                    System.out.printf("\tNAME: %s", a.getName());
                }
                trackCount++;
            }
        }

        if (trackCount == 0) {
            System.out.println("\nNo tracks found with that name");
        }

        userInput.goBack();
    }

    public void printHeader(){
        System.out.println("\t>----------------<");
        System.out.println("\t>   Track List   <");
        System.out.println("\t>----------------<");
    }
}

