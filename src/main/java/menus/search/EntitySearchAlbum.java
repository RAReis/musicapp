package menus.search;

import menus.BaseMenu;
import menus.UserInput;
import models.Album;
import services.AlbumService;

import java.util.List;

public class EntitySearchAlbum extends BaseMenu {
    private UserInput userInput = new UserInput();
    private List<Album> albums = AlbumService.getAlbumList();

    public void chooseAndPrintAlbum(){
        clearScreen();
        printHeader();
        if (albums.isEmpty()) {
            System.out.println("\n\nNo Albums found");
            userInput.goBack();
            return;
        }
        showAlbumList();

        int albumCount = 0;

        System.out.print("\nChoose one using Name: ");
        String name = userInput.getName();

        for (Album album : albums) {
            if (album.getName().toLowerCase().contains(name.toLowerCase())) {
                System.out.printf("\nAlbum Id: %d Album Name: %s", album.getId(), album.getName());
                albumCount++;
            }
        }

        if (albumCount == 0) {
            System.out.println("No Albums found with that name");
        }

        userInput.goBack();
    }

    public void showAlbumList(){
       printHeader();

        if (albums.isEmpty()) {
            System.out.println("\n\nNo Albums found");
            return;
        }

        /** Sort using Id **/ //TODO understand the lambda part
        albums.sort((o1, o2) -> {
            if (o1.getId() == o2.getId()) {
                return 0;
            } else if (o1.getId() > o2.getId()) {
                return 1;
            } else {
                return -1;
            }
        });


        for (Album album : albums) {
            System.out.printf("\n[ Album ID: %d ]\n Album NAME: %s\n ", album.getId(), album.getName());
        }
    }

    public void printHeader(){
        System.out.println("\t>----------------<");
        System.out.println("\t>   Album List   <");
        System.out.println("\t>----------------<");
    }
}
