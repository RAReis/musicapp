package menus;

import menus.create.EntityCreatorMenu;

public class MainMenu extends BaseMenu{

    private UserInput userInput = new UserInput();
    private EntityCreatorMenu entityCreatorMenu = new EntityCreatorMenu();
    private EntityViewDetailsMenu entityViewDetailsMenu = new EntityViewDetailsMenu();
    private EntityViewSortedAlbumsMenu entityViewSortedAlbumsMenu = new EntityViewSortedAlbumsMenu();
    private EntitySearchMenu entitySearchMenu = new EntitySearchMenu();

    private int menuOptions = 4;

    public void printMainMenu(){

        clearScreen();

        System.out.println("\t+--------------+");
        System.out.println("\t|              |");
        System.out.println("\t|     MENU     |");
        System.out.println("\t|              |");
        System.out.println("\t+--------------+\n");

        System.out.println("Select one option: \n");
        System.out.println(" 1) Create Album , Artist or Track");
        System.out.println(" 2) Show Track or Album details");
        System.out.println(" 3) Show Albums by year or genre");
        System.out.println(" 4) Search by Name");
        System.out.println(" 0) Exit");

        int choice = userInput.getChoice(menuOptions);

        handleUserChoice(choice);

    }

    private void handleUserChoice(int choice) {

        clearScreen();
        switch (choice) {
            case 0:
                System.out.println("Closing App");
                return;

            case 1:
                entityCreatorMenu.handleEntityCreation();
                printMainMenu();
                return;

            case 2:
                entityViewDetailsMenu.showTrackAlbumDetails();
                printMainMenu();
                return;

            case 3:
                entityViewSortedAlbumsMenu.showSortedAlbums();
                printMainMenu();
                return;

            case 4:
                entitySearchMenu.searchEntities();
                printMainMenu();
                return;
        }
    }
}


