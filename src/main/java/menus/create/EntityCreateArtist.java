package menus.create;

import services.ArtistService;
import menus.BaseMenu;
import menus.UserInput;
import models.Artist;

public class EntityCreateArtist extends BaseMenu {
    private UserInput userInput = new UserInput();

    private Artist artist;
    private String name;


    public void handleArtistCreation() {
        clearScreen();
        printHeader();
        System.out.print("\nEnter Artist name: ");
        name = userInput.getName();
        artist = new Artist(name);
        ArtistService.addArtist(artist);
        System.out.println("\nArtist successfully created");
        userInput.goBack();
    }


    public void printHeader(){
        System.out.println("\t>---------------<");
        System.out.println("\t> Create Artist <");
        System.out.println("\t>---------------<\n");
    }
}
