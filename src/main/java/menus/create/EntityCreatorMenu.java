package menus.create;

import menus.BaseMenu;
import menus.UserInput;

public class EntityCreatorMenu extends BaseMenu {

    private UserInput userInput = new UserInput();
    private EntityCreateTrack entityCreateTrack = new EntityCreateTrack();
    private EntityCreateArtist entityCreateArtist = new EntityCreateArtist();
    private EntityCreateAlbum entityCreateAlbum = new EntityCreateAlbum();

    private int menuOptions = 3;

    public void printMenu() {
        clearScreen();

        System.out.println("\t>--------------<");
        System.out.println("\t> Creator Menu <");
        System.out.println("\t>--------------<\n");

        System.out.println("Select one option: ");
        System.out.println(" 1) Create Album");
        System.out.println(" 2) Create Artist");
        System.out.println(" 3) Create Track");
        System.out.println(" 0) <- Back");

    }

    public void handleEntityCreation(){
        printMenu();
        int choice = userInput.getChoice(menuOptions);

        switch (choice) {
            case 0:
                System.out.println("<- back\n");
                return;

            case 1:
                entityCreateAlbum.handleAlbumCreation();
                handleEntityCreation();
                return;

            case 2:
                entityCreateArtist.handleArtistCreation();
                handleEntityCreation();
                return;

            case 3:
                entityCreateTrack.handleTrackCreation();
                handleEntityCreation();
                return;
        }
    }
}

