package menus.create;

import services.AlbumService;
import menus.BaseMenu;
import menus.UserInput;
import models.Album;

public class EntityCreateAlbum extends BaseMenu {
    private UserInput userInput = new UserInput();

    private Album album;
    private String name;
    private int year;

    public void handleAlbumCreation() {
        clearScreen();
        printHeader();
        System.out.print("\nEnter Album name: ");
        name = userInput.getName();
        System.out.print("\nEnter Album year: ");
        year = userInput.getInt();

        album = new Album(year, name);
        AlbumService.addAlbum(album);
        System.out.println("\nAlbum successfully created");
        userInput.goBack();
    }

    public void printHeader(){
        System.out.println("\t>--------------<");
        System.out.println("\t> Create Album <");
        System.out.println("\t>--------------<\n");
    }
}
