package menus.create;

import menus.BaseMenu;
import menus.UserInput;
import menus.search.EntitySearchAlbum;
import models.Album;
import models.Artist;
import models.Track;
import services.AlbumService;
import services.ArtistService;
import services.TrackService;

import java.util.LinkedList;
import java.util.List;

public class EntityCreateTrack extends BaseMenu {
    private UserInput userInput = new UserInput();
    private EntitySearchAlbum entitySearchAlbum = new EntitySearchAlbum();

    private Track track;

    private String name;
    private int duration; // in seconds
    private String genre;
    private List<Artist> artistList;


    public void handleTrackCreation() {
        clearScreen();
        printHeader();
        if(ArtistService.getArtistList().isEmpty()){
            System.out.println("Create an Artist first before creating a Track");
            userInput.goBack();
            return;
        }
        getTrackName();
        getTrackDuration();
        getTrackGenre();
        getTrackArtists();
        track = new Track(name, duration, genre, artistList);
        addTrackToAlbum(track);
        for (Artist a : track.getArtistList()) {
            if (!a.getTrackList().contains(track)) {
                a.addTrackToArtist(track);
            }
        }

        /** Add track to list of all the tracks **/
        TrackService.addTrack(track);
    }


    private void getTrackName() {
        System.out.print("Enter the name of the track: \n");
        name = userInput.getName();
    }

    private void getTrackDuration() {
        System.out.println("\nEnter the duration of the track (in seconds): ");
        duration = userInput.getDuration();
    }

    private void getTrackGenre() {
        System.out.println("\nEnter the genre of the track: ");
        genre = userInput.getName();
    }

    private void getTrackArtists() {
        int numberOfArtists;
        int count;

        System.out.println("\nEnter the number of artists you want to add to the track (at least one): ");
        numberOfArtists = userInput.getNumberOfArtists();
        count = 1;
        artistList = new LinkedList<>();

        ArtistService.printArtists();
        while (count <= numberOfArtists) {
            int inputID;

            do {
                System.out.printf("\nChoose Artist %d by ID: ", count);
                inputID = userInput.getInt();
                if (inputID <= 0 || inputID > ArtistService.getArtistList().size()) {
                    System.out.print("\n Artist ID not found. Try Again\n");
                }
                Artist chosenArtist = ArtistService.getArtistById(inputID);
                //don't add repeated artists
                if (!artistList.contains(chosenArtist)) {
                    artistList.add(chosenArtist);
                }

            } while (inputID <= 0 || inputID > ArtistService.getArtistList().size());

            count++;
        }
    }

    private void addTrackToAlbum(Track track) {
        System.out.println("\nDo you wanna add this track to an Album?");

        System.out.println("\nPress Y / Yes or N / No");
        String inputAnswer = userInput.getName();
        if (inputAnswer.toLowerCase().equals("y") || inputAnswer.toLowerCase().equals("yes")) {
            clearScreen();
            if(AlbumService.getAlbumList().isEmpty()){
                System.out.println("\nNo Albums found");
                userInput.goBack();
                return;
            }
            entitySearchAlbum.showAlbumList();
            addToAlbumById(track);

        } else if (inputAnswer.toLowerCase().equals("n") || inputAnswer.toLowerCase().equals("no")) {
            userInput.goBack();

        } else {
            System.out.println("\nPlease select one of the options.");
            addTrackToAlbum(track);

        }
    }

    private void addToAlbumById(Track track) {
        int albumCount = 0;

        System.out.print("\nChoose one using ID: ");
        int id = userInput.getInt();

        for (Album album : AlbumService.getAlbumList()) {
            if (album.getId() == id) {
                album.addTrackToAlbum(track);
                track.addToAlbumList(album);
                albumCount++;
                System.out.printf("\nTrack successfully added to Album %s", album.getName());
            }
        }

        if (albumCount == 0) {
            System.out.printf("\nFailed attempt. No Albums found with Id: %d", id);
            addToAlbumById(track);
        }

        userInput.goBack();
    }

    private void printHeader() {
        System.out.println(">---------------<");
        System.out.println("> Track Creator <");
        System.out.println(">---------------<\n");
    }
}
