package menus;

import menus.show.ShowByGenre;
import menus.show.ShowByYear;

public class EntityViewSortedAlbumsMenu extends BaseMenu {

    private UserInput userInput = new UserInput();
    private ShowByYear showByYear = new ShowByYear();
    private ShowByGenre showByGenre = new ShowByGenre();

    private int menuOptions = 2;

    public void printMenu() {
        clearScreen();

        System.out.println("\t>---------------<");
        System.out.println("\t>  Albums Menu  <");
        System.out.println("\t>---------------<\n");

        System.out.println("Select one option: ");
        System.out.println(" 1) Show by Year");
        System.out.println(" 2) Show by Genre");
        System.out.println(" 0) <- Back");

    }

    public void showSortedAlbums() {
        clearScreen();
        printMenu();
        int choice = userInput.getChoice(menuOptions);

        switch (choice) {
            case 0:
                System.out.println("<- back\n");
                return;

            case 1:
                showByYear.printSortedByYear();
                showSortedAlbums();
                return;

            case 2:
                showByGenre.printSortedByGenre();
                showSortedAlbums();
                return;

        }
    }

}
