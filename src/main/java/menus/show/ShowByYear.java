package menus.show;

import menus.BaseMenu;
import menus.UserInput;
import models.Album;
import services.AlbumService;

import java.util.List;

public class ShowByYear extends BaseMenu {
    private UserInput userInput = new UserInput();

    public void printSortedByYear() {
        clearScreen();
        printHeader();
        List<Album> albums = AlbumService.getAlbumList();

        if (albums.isEmpty()) {
            System.out.println("No albums found");
            userInput.goBack();
            return;
        }
        albums.sort((o1, o2) -> {
            if (o1.getYear() == o2.getYear()) {
                return 0;
            } else if (o1.getYear() > o2.getYear()) {
                return -1;
            } else {
                return 1;
            }
        });

        for (Album album : albums) {
            System.out.printf("[ Album YEAR: %d ] \nAlbum NAME: %s \nAlbum ID: %d\n", album.getYear(), album.getName(), album.getId());
        }

        userInput.goBack();
    }

    public void printHeader() {
        System.out.println("\t>----------------<");
        System.out.println("\t> Albums by Year <");
        System.out.println("\t>----------------<\n");
    }
}
