package menus.show;

import services.AlbumService;
import menus.UserInput;
import models.Album;

public class ShowAlbumDetails {
    private UserInput userInput = new UserInput();

    public void showAlbumDetails() {
        printHeader();

        if (AlbumService.getAlbumList().isEmpty()) {
            System.out.println("\nNo Albums found");
        } else {
            AlbumService.printAlbums();
            System.out.print("\nEnter Album Name: ");
            String name = userInput.getName();
            int albumCount = 0;

            for (Album album : AlbumService.getAlbumList()) {
                if (album.getName().toLowerCase().contains(name.toLowerCase())) {
                    System.out.printf("\n[ Album ID: %d ]\n NAME: %s\n YEAR: %d", album.getId(), album.getName(), album.getYear());
                    album.printTrackList();
                    albumCount++;
                }
            }
                if (albumCount == 0) {
                    System.out.println("\nNo Albums found with that name");
                }

        }
        userInput.goBack();
    }

    public void printHeader(){
        System.out.println("\t>---------------<");
        System.out.println("\t> Album Details <");
        System.out.println("\t>---------------<\n");
    }
}
