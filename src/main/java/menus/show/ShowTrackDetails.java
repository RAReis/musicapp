package menus.show;

import menus.BaseMenu;
import menus.UserInput;
import models.Track;
import services.TrackService;

public class ShowTrackDetails extends BaseMenu {
    private UserInput userInput = new UserInput();

    public void showTrackDetails() {
        clearScreen();
        header();

        if (TrackService.getTrackList().isEmpty()) {
            System.out.println("\nNo Tracks found");

        } else {
            TrackService.printTracks();
            System.out.print("\nEnter Track name: ");
            String name = userInput.getName();
            int trackCount = 0;

            for (Track t : TrackService.getTrackList()) {
                if (t.getName().toLowerCase().contains(name.toLowerCase())) {
                    System.out.printf("\n [ Track ID: %d ]\n NAME: %s\n GENRE: %s\n", t.getId(), t.getName(), t.getGenre());
                    t.printArtistList();
                    t.printAlbumList();
                    System.out.println();
                    trackCount++;
                }
            }

            if (trackCount == 0) {
                System.out.println("No Tracks found with that name");
            }

        }
        userInput.goBack();
    }


    public void header(){
        System.out.println("\t>---------------<");
        System.out.println("\t> Track Details <");
        System.out.println("\t>---------------<\n");
    }
}
