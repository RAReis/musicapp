package menus.show;

import menus.BaseMenu;
import menus.UserInput;
import models.Album;
import services.AlbumService;

import java.util.LinkedList;
import java.util.List;

public class ShowByGenre extends BaseMenu {
    private UserInput userInput = new UserInput();

    public void printSortedByGenre() {
        clearScreen();
        printHeader();
        List<Album> albums = AlbumService.getAlbumList();

        if (albums.isEmpty()) {
            System.out.println("No albums found");
            userInput.goBack();
            return;
        }

        //add all genres in all albums to a list
        List<String> genreList = new LinkedList<>();
        for (Album a : albums) {
            for (String genre : a.getGenreList()) {
                //don't repeat genres
                if (!genreList.contains(genre)) {
                    genreList.add(genre);
                }
            }
        }

        for (String genre : genreList) {
            System.out.printf("\nGenre: %s", genre);
        }

        System.out.println("\n\nChoose a Genre: ");
        String name = userInput.getName();
        System.out.printf("\n[ Genre %s ]\n", name.toUpperCase());

        int count = 0;
        for (Album a : albums) {
            if (a.getGenreList().contains(name.toLowerCase())) {
                System.out.printf("\n [ Album ID: %d ]\n ALBUM NAME: %s \n", a.getId(), a.getName());
                count++;
            }
        }

        if(count == 0){
            System.out.println("\nNo Albums found for this Genre");
        }

        userInput.goBack();
    }

    public void printHeader() {
        System.out.println("\t>-----------------<");
        System.out.println("\t> Albums by Genre <");
        System.out.println("\t>-----------------<\n");
    }
}
