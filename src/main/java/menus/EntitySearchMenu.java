package menus;

import menus.search.EntitySearchAlbum;
import menus.search.EntitySearchArtist;
import menus.search.EntitySearchTrack;

public class EntitySearchMenu extends BaseMenu {

    private UserInput userInput = new UserInput();
    private EntitySearchAlbum entitySearchAlbum = new EntitySearchAlbum();
    private EntitySearchArtist entitySearchArtist = new EntitySearchArtist();
    private EntitySearchTrack entitySearchTrack = new EntitySearchTrack();

    private int menuOptions = 3;

    public void printMenu() {
        clearScreen();

        System.out.println("\t>---------------<");
        System.out.println("\t>  Search Menu  <");
        System.out.println("\t>---------------<\n");

        System.out.println("Select one option: ");
        System.out.println(" 1) Search Album");
        System.out.println(" 2) Search Artist");
        System.out.println(" 3) Search Track");
        System.out.println(" 0) <- Back");

    }

    public void searchEntities() {
        printMenu();
        int choice = userInput.getChoice(menuOptions);

        switch (choice) {
            case 0:
                System.out.println("<- back\n");
                return;

            case 1:
                entitySearchAlbum.chooseAndPrintAlbum();
                searchEntities();
                return;

            case 2:
                entitySearchArtist.showAndChooseArtist();
                searchEntities();
                return;
            case 3:
                entitySearchTrack.showAndChooseTrack();
                searchEntities();
                return;
        }
    }


}
