package menus;

import models.Artist;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class UserInput {
    Scanner sc;

    public int getChoice(int options) {
        sc = new Scanner(System.in);
        int choice = Integer.MAX_VALUE;
        boolean passed;


        do {
            passed = true;
            while (!sc.hasNextInt()) {
                System.out.println("Numbers only, please. Try again");
                sc.next();
            }

            choice = sc.nextInt();

            if (choice > options || choice < 0) {
                System.out.println("Option not available. Try again");
                passed = false;
            }

        } while (!passed);

        return choice;

    }

    public String getName() {
        sc = new Scanner(System.in);
        String string = "";
        do {
            string = sc.nextLine();
            if (string.length() <= 0) {
                System.out.println("Invalid name!\nTryAgain: ");
            }
        } while (string.length() <= 0);

        return string;
    }

    public int getDuration() {
        sc = new Scanner(System.in);
        int duration = -1;

        do {
            try {
                duration = sc.nextInt();
                if (duration <= 0) {
                    System.out.println("Duration must be more than 0 seconds\nTry again: ");
                    duration = -1;
                }
            } catch (InputMismatchException exception) {
                System.out.println("Numbers only, please. Try again");
                getDuration();
            }
        } while (duration <= 0);

        return duration;
    }

    public int getNumberOfArtists() {
        sc = new Scanner(System.in);
        int amount = Integer.MIN_VALUE;

        do {
            try {
                amount = sc.nextInt();
                if (amount <= 0) {
                    System.out.print("\nAt least one artist must be added\nTry again:");
                }
            } catch (InputMismatchException exception) {
                System.out.println("\nNumbers only, please.\nTry again: ");
                goBack();
            }
        } while (amount <= 0);

        return amount;
    }

    public List<Artist> getArtists(int numberOfArtists) {
        List<Artist> artistList = new LinkedList<>();
        Artist artist;
        do {
            System.out.printf("Artist %d :", numberOfArtists);
            artist = new Artist(getName());
            artistList.add(artist);
            numberOfArtists--;
        } while (numberOfArtists > 0);

        return artistList;
    }

    public int getInt() {
        sc = new Scanner(System.in);

        int number;

        while (!sc.hasNextInt()) {
            System.out.println("Numbers only, please. Try again");
            sc.next();
        }

        number = sc.nextInt();

        return number;
    }

    public void goBack() {
        System.out.print("\n\nPress \"ENTER\" key to go back\n");
        sc = new Scanner(System.in);
        sc.nextLine();
    }

}
