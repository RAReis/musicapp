package menus;

import menus.show.ShowAlbumDetails;
import menus.show.ShowTrackDetails;

public class EntityViewDetailsMenu extends BaseMenu {

    private UserInput userInput = new UserInput();
    private ShowAlbumDetails albumDetails = new ShowAlbumDetails();
    private ShowTrackDetails trackDetails = new ShowTrackDetails();

    private int menuOptions = 2;

    public void printMenu() {
        clearScreen();

        System.out.println("\t>--------------<");
        System.out.println("\t> Details Menu <");
        System.out.println("\t>--------------<\n");

        System.out.println("Select one option: ");
        System.out.println(" 1) Show Track details");
        System.out.println(" 2) Show Album details");
        System.out.println(" 0) <- Back");

    }

    public void showTrackAlbumDetails() {
        printMenu();
        int choice = userInput.getChoice(menuOptions);

        switch (choice) {
            case 0:
                System.out.println("<- back\n");
                return;

            case 1:
                trackDetails.showTrackDetails();
                showTrackAlbumDetails();
                return;

            case 2:
                clearScreen();
                albumDetails.showAlbumDetails();
                showTrackAlbumDetails();
                return;
        }
    }
}
