##  Music App

###  What you need:
- Java
- Maven

### Installing the App:
- Clone repository from https://bitbucket.org/RAReis/musicapp

### How to run it:
- Go to the directory were you saved the App (ex: cd musicApp);
- Enter the command "msn clean package" (to compile);
- From here go to target/ (cd target/) and enter the command
    "java -jar musicApp-1.0.jar"
- That's it!

#### Go to "Documentation" folder to see ER Model and Menu Diagram